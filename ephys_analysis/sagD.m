function [SagVat200 ,SagRat200, Rebat200] = sagD(nSweeps);


%% File load

[fileName,pathName,] = uigetfile ('*.*', 'Select the IV traces');
load(fullfile(pathName,fileName));

%nSweeps = 15; %number of recorded sweeps
lenghtSweep = 25000;
RmSweep = zeros(lenghtSweep,nSweeps);

%rearrange sweeps in one matrix
traces= who('Trace_*'); %identify loaded files from DAT
traceName = traces{1}; %name of a trace
startTraceName=traceName(1:10); %first part of trace name
for ind = 1:nSweeps,
    temp = eval(strcat(startTraceName,num2str(ind),'_2')); %single sweep of 700ms
    RmSweep(:,ind)= temp(1:lenghtSweep,2);
end

inputfile = sgolayfilt(RmSweep,3,91); %filtered sweep traces

%% Parameters
plotting = 1; %select 1 yes and 0 for no

si_ms = 0.1; % in ms DAVIDE
Fs = 10; %in Khz
currInjDuration = 1000; %current injection duration in ms
sweepTime = (0:length(inputfile)-1).*si_ms; %time of each sweep (in ms)
base = 45; % baseline time (in ms)
currInjEnd = base + currInjDuration; %time of end of current injection (in ms)
minSearch = 145; %time to search for minimum point after baseline (in ms)
SteadyStateSearch = 500; %time to average to measure sag amplitude at the end of the current injection (in ms)
rebSearch = 300; %time to search for maximum point for rebound (in ms)

%% Sag and rebound depolarization

SagAllSweeps = zeros(2,nSweeps); %matrix with sag for all sweeps
RebAllSweeps = zeros(1,nSweeps); %matrix with sag for all sweeps

figure,
for ind =1:nSweeps,

    SagSweepTrace = inputfile(:,ind)*1000;
    minHyperpSearchTrace = SagSweepTrace(base*Fs:(minSearch*Fs));  % part of trace to search for min hyperpolarization point
    SteadyState = SagSweepTrace(round(currInjEnd*Fs)-(round(SteadyStateSearch/2)*Fs):round(currInjEnd*Fs)-(round(base)*Fs)); % part of trace to search for sag (end of hyperp step)
    rebSearchTrace = SagSweepTrace(round(currInjEnd*Fs):round(currInjEnd*Fs+(rebSearch*Fs))); %part of trace to search for rebound depolarization

    SteadyTraceMedian = median(SteadyState); % MEDIAN sag value (membrane value, not amplitude)
    baseMedian = median(SagSweepTrace(1:base*Fs)); % MEDIAN baseline at start
    endMedian = median(SagSweepTrace((currInjEnd+rebSearch)*Fs:(currInjEnd+rebSearch*2)*Fs)); % MEDIAN baseline at end

    minHyperpValue = min(minHyperpSearchTrace); % mean min hyperpolarization value ((membrane value, not amplitude)
    maxRebValue = max(rebSearchTrace); % mean max rebound depolariz value (membrane value, not amplitude)
    minHyperpLoc = find(minHyperpSearchTrace==minHyperpValue,1); % location of min hyperpolarization value
    maxRebLoc = find(rebSearchTrace==maxRebValue,1);  % location of max rebound depolarization value

    %minHyperpMean = mean(minHyperpSearchTrace(minHyperpLoc-1:minHyperpLoc+1)); % mean of 3 data points around the min hyperpolariz value
    %maxRebMean = mean(rebSearchTrace(maxRebLoc-1:maxRebLoc+1)); % mean of 3 data points around the max rebound depolariz value

    hyperpAmplStart = baseMedian - minHyperpValue; % hyperpolarization amplitude at the beginning of the current injection (HCN channels are closed)
    hyperpAmplEnd = baseMedian - SteadyTraceMedian; % hyperpolarization amplitude at the end of the current injection (HCN channels are open)
    sagRatio = hyperpAmplEnd / hyperpAmplStart; % sag ratio: values closer to zero imply stronger sag
    sagValue = minHyperpValue - SteadyTraceMedian; %sag value in mV
    rebAmpl = (maxRebValue - endMedian); %rebound amplitude

    SagAllSweeps(1,ind) = sagRatio;
    SagAllSweeps(2,ind) = sagValue;
    RebAllSweeps(1,ind) = rebAmpl;

    minHyperpLocFullSweep = minHyperpLoc + (base*Fs);
    maxRebLocFullSweep = maxRebLoc + (currInjEnd*Fs);

    if maxRebValue > 0;
       warning('WARNING: rebound spike!')
       RebAllSweeps(1,ind) = NaN;
    end





    %% Plotting
    
    if plotting == 1; %if plotting is set on 'yes'
    subplot(221), plot(SagSweepTrace);
    hold on;
    scatter(minHyperpLocFullSweep,minHyperpValue,100,'r');
    scatter(maxRebLocFullSweep,maxRebValue,100,'r');
    title('Sag and rebound depolarization')
    ylabel('Membrane potential (mV)')
    xlabel('Data points')


    else
    end
  
end

    subplot(222), scatter((1:nSweeps)*20,RebAllSweeps);
    title('I vs Rebound depolarization')
    ylabel('(mV)')
    xlabel('Current Step')
    
    subplot(223), scatter((1:nSweeps)*20,SagAllSweeps(1,:));
    title('I vs Sag Ratio')
    %ylabel('')
    xlabel('Current Step')

    subplot(224), scatter((1:nSweeps)*20,SagAllSweeps(2,:));
    title('I vs Sag Value')
    ylabel('(mV)')
    xlabel('Current Step')

    
SagVat200 = SagAllSweeps(2,10);
SagRat200 = SagAllSweeps(1,10);
Rebat200= RebAllSweeps(10);    

%% Printing results
sagRatioDisplay = ['Sag ratio = ', num2str(SagAllSweeps(1,10))];
disp (sagRatioDisplay);
sagValueDisplay = ['Sag value = ', num2str(SagAllSweeps(2,10)), ' mV'];
disp (sagValueDisplay);
rebAmplDisplay = ['Rebound amplitude = ', num2str(RebAllSweeps(10)), ' mV'];
disp (rebAmplDisplay);

end

