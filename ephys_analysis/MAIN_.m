%%MAIN SCRIPT

clear all
close all

cd(strcat('C:\Users\cavalieri\Documents\INMED\Experiments', ...
    '\Patch Clamp Experiments\CC\RsComp_OK\Traces\E14\2018-04-27\rec02\'));

si_ms = 0.1; % in ms DAVIDE
Fs = 10; %in Khz

%%
startingFit = [-1 10 1 20 -70]; %initial guesses for the exponential 
[inputRes membraneTau membraneCapacitance ccstepFirstSpike fileName] = ccstepD(Fs,startingFit);


%%
[SagV sagRatio rebAmpl] = sagD(20);

%%
[inputoutput, inputoutputTimes, firstSpikeAmpl fAHPAmpl spikeThreshold spikeHalfWidth] ...
    = ccFiringD(1,Fs,25);

% bigger than 500 ms
sweepFraction = 500; 
spikeFraction = sum(inputoutputTimes < sweepFraction);
figure, plot(spikeFraction./inputoutput,'-o')

   
%% Saving results
% %arrayToSave = [rmpValue', inputRes', membraneTau', membraneCapacitance', spikeThreshold', firstSpikeAmpl', spikeHalfWidth', fAHPAmpl', sagRatio', rebAmpl', firingRate1', maxFiringRate1', adaptIndex', burstIndex', isiCV', NaN, inputoutput, NaN, inputoutputBurst, NaN, burstIndexCurve];
% 
% arrayToSave = [inputRes', membraneTau', membraneCapacitance', spikeThreshold', firstSpikeAmpl', spikeHalfWidth', fAHPAmpl', sagRatio', rebAmpl', firingRate1', maxFiringRate1', adaptIndex', burstIndex', isiCV', NaN, inputoutput, NaN, inputoutputBurst, NaN, burstIndexCurve];
% 
% %dlmwrite(['/Users/marco_bocchio/Documents/MATLAB/Results/' fileName '.txt'], arrayToSave);
% %arrayToSave = vertcat({'RMP', 'Rin', 'membraneTau', 'membraneCapacitance', 'spikeThreshold', 'firstSpikeAmpl', 'spikeHalfWidth', 'fAHPAmpl', 'sagRatio', 'rebAmpl', 'firingRate', 'maxFiringRate', 'adaptIndex', 'inputoutput'}, arrayToSave);
% csvwrite(['D:/Documents/MATLAB/Results/Intrinsic membrane properties/' fileName '.csv'], arrayToSave);
% 
% 
% 
% %end
