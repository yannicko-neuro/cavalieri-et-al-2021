function  [firstSpikeAmpl fAHPAmpl spikeThreshold spikeHalfWidth] = firstSpikeD(Fs,firstSweepWithSpikes,inputfile, firstSpikePeak, firstSpikePeakLoc)  

%% Parameters

firstSpikeWindow = 30; %time window that is taken to quantify and plot the first spike in the IV (in ms)
fAHPwindow=5; %window after AP peak to look for fast AHP
si_ms=1/Fs; %sampling interval in ms
sweepTime = (0:length(inputfile)-1).*si_ms; %time of each sweep (in ms)

%%
% Calculates spike threshold, half-width, amplitude and after-hyperpolarization


    % Threshold with 2nd derivative
    firstSpikeTrace = inputfile(firstSpikePeakLoc-(firstSpikeWindow/2*Fs):firstSpikePeakLoc+(firstSpikeWindow/2*Fs),firstSweepWithSpikes); %location of the first spike in the first sweep with spikes
    firstSpikeTraceMinus1 = firstSpikeTrace(1:end-1); %spike waveform minus last point to plot against derivative
    firstSpikeTraceMinus2 = firstSpikeTrace(1:end-2); %spike waveform minus last point to plot against derivative

    timeFirstSpikeTrace = ((0:(length(firstSpikeTrace))-1)./Fs)'; %time vector for first spike waveform

    diffFirstSpikeTrace = diff(firstSpikeTrace); %derivative of first spike
    diff2FirstSpikeTrace = diff(diff(firstSpikeTrace)); %derivative of first spike
    %[~,spikeThresholdLoc]=findpeaks(diff2FirstSpikeTrace,'SORTSTR','descend','NPEAKS',1); %filter to avoid double peaks
    [~,spikeThresholdLoc]=findpeaks(medfilt1(diff2FirstSpikeTrace,3),'SORTSTR','descend','NPEAKS',1); %filter to avoid double peaks
    spikeThreshold = firstSpikeTraceMinus2(spikeThresholdLoc); %value of spike threshold
    spikeThresholdTime = (spikeThresholdLoc)/Fs;

    

    % Amplitude
    firstSpikeAmpl = (firstSpikePeak-spikeThreshold); %amplitude of first spike
    
    % fAHP (old method)    
    [~,tempPeakLoc]=findpeaks(firstSpikeTrace,'SORTSTR','descend','NPEAKS',1); %find relative time of spike in the trace
    fAHPcut = -firstSpikeTrace(tempPeakLoc+1:tempPeakLoc+1+fAHPwindow*Fs); %isolate fragment for fAHP (from spike peak)
    [realfAHPValue fAHPLoc] = findpeaks(fAHPcut,'SORTSTR','descend','NPEAKS',1); %finds fAHP in 5ms after peak
    fAHPValue = -mean(fAHPcut(fAHPLoc-1:fAHPLoc+1)); %mean of 3 point for AHP calculation (- for inverted trace)
    fAHPTime = find(firstSpikeTrace(1:tempPeakLoc+1+fAHPwindow*Fs)==-realfAHPValue,1)/ Fs; %time of data point to measure fAHP
    fAHPAmpl = spikeThreshold-(fAHPValue); %fAHP amplitude

    % Half-width
    firstSpikeHalfAmpl=firstSpikeAmpl/2; %first spike half amplitude
    
    firstSpikeTracePeakLoc = find(firstSpikeTrace==firstSpikePeak); %peak location in the spike waveform vector
    firstSpikeTraceUntilPeak = firstSpikeTrace(1:firstSpikeTracePeakLoc); %trace containing the first spike only until the spike peak (to calculate half amplitude in the ascending phase only)

    %Ascending phase
    [firstSpikeHalfAmpl1Delta firstSpikeTraceHalfAmpl1Loc] = min(abs(firstSpikeTraceUntilPeak-(spikeThreshold+firstSpikeHalfAmpl))); %point of the waveform corresponding to half amplitude
    firstSpikeTraceHalfAmpl1 = firstSpikeTraceUntilPeak(firstSpikeTraceHalfAmpl1Loc); %trace value corresponding to half amplitude in the ascending phase of the spike
    firstSpikeTraceHalfAmpl1Time = firstSpikeTraceHalfAmpl1Loc / Fs; %time of half amplitude data point (ascending phase)

    %Descending phase
    [firstSpikeHalfAmpl2Delta firstSpikeDescHalfAmpl2Loc] = min(abs(firstSpikeTrace(firstSpikeTracePeakLoc:end)-firstSpikeTraceHalfAmpl1)); %point of the waveform corresponding to half amplitude
    firstSpikeDescHalfAmpl2Loc = firstSpikeDescHalfAmpl2Loc-1; %take data point before in the trace as usually it is more accurate
    firstSpikeTraceHalfAmpl2 = firstSpikeTrace(firstSpikeTracePeakLoc+firstSpikeDescHalfAmpl2Loc); %trace value corresponding to half amplitude
    firstSpikeTraceHalfAmpl2Loc = (firstSpikeDescHalfAmpl2Loc + firstSpikeTracePeakLoc); %trace value corresponding to half amplitude in the descending phase of the spike
    firstSpikeTraceHalfAmpl2Time = (firstSpikeDescHalfAmpl2Loc + firstSpikeTracePeakLoc) / Fs; %time of half amplitude data point (descending phase)
    spikeHalfWidth = firstSpikeTraceHalfAmpl2Time - firstSpikeTraceHalfAmpl1Time; %spike half-width in ms


%% Plotting
    figure,
 % Spikes
    subplot(2,2,1);
    plot(sweepTime,inputfile(:,firstSweepWithSpikes));
    hold;
    scatter(sweepTime(firstSpikePeakLoc),firstSpikePeak,50,'r')
    xlim([0 max(sweepTime)]);
    title('First spike detection')

    % First spike
    subplot(2,2,2);
    plot(timeFirstSpikeTrace+1/Fs,firstSpikeTrace)
    xlim([min(timeFirstSpikeTrace) max(timeFirstSpikeTrace)]);
    hold on;
    scatter(spikeThresholdTime,spikeThreshold,100,'r');
    scatter(firstSpikeTraceHalfAmpl1Time,firstSpikeTraceHalfAmpl1,100,'r');
    scatter(firstSpikeTraceHalfAmpl2Time,firstSpikeTraceHalfAmpl2,100,'r');
    scatter(fAHPTime,-realfAHPValue,100,'r');
    title('First spike')
    hold off;


    subplot(2,2,3);
    plot(firstSpikeTraceMinus1,diffFirstSpikeTrace);
    title('First spike dV/V')
    %xlim([diffFirstSpikeTrace(1) max(diffFirstSpikeTrace)]);
    %ylim([firstSpikeTrace(1) max(firstSpikeTrace)]);


    % Derivatives
    subplot(2,2,4);
    plot(timeFirstSpikeTrace(1:end-2)+1/Fs,firstSpikeTraceMinus2-median(firstSpikeTrace),'k')
    hold on; xlim([10 20])
    plot(timeFirstSpikeTrace(1:end-2)+1/Fs,diffFirstSpikeTrace(1:end-1),'b')
    plot(timeFirstSpikeTrace(1:end-2)+1/Fs,diff2FirstSpikeTrace,'r')
    plot([spikeThresholdLoc/Fs spikeThresholdLoc/Fs],ylim,'g--')
    title('First spike trace and its derivatives')
    legend('Spike Trace','1st Derivative','2nd Derivative','Max 2nd Derivative','Location','northwest')
    hold off;  
    
    %% Print results
    
    
    %% Printing results   
    spikeThresholdDisplay = ['Spike threshold = ', num2str(spikeThreshold), ' mV'];
    disp (spikeThresholdDisplay);
    spikeAmplDisplay = ['Spike amplitude = ', num2str(firstSpikeAmpl), ' mV'];
    disp (spikeAmplDisplay);
    spikeHalfWidthDisplay = ['Spike half-width = ', num2str(spikeHalfWidth), ' ms'];
    disp (spikeHalfWidthDisplay);
    fAHPAmplDisplay = ['After-hyperpolarization amplitude = ', num2str(fAHPAmpl), ' mV'];
    disp (fAHPAmplDisplay);

end