function varargout = untitled2(varargin)
% UNTITLED2 MATLAB code for untitled2.fig
%      UNTITLED2, by itself, creates a new UNTITLED2 or raises the existing
%      singleton*.
%
%      H = UNTITLED2 returns the handle to a new UNTITLED2 or the handle to
%      the existing singleton*.
%
%      UNTITLED2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UNTITLED2.M with the given input arguments.
%
%      UNTITLED2('Property','Value',...) creates a new UNTITLED2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before untitled2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to untitled2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help untitled2

% Last Modified by GUIDE v2.5 30-Aug-2018 01:09:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @untitled2_OpeningFcn, ...
                   'gui_OutputFcn',  @untitled2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before untitled2 is made visible.
function untitled2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to untitled2 (see VARARGIN)

% Choose default command line output for untitled2
handles.output = hObject;
handles.currdat=evalin('base','vals6');
handles.M=median(handles.currdat(:));
handles.I=iqr(handles.currdat(:));
handles.n=0;
handles.minov=1;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes untitled2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = untitled2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.ii=round(get(hObject,'Value'));


if handles.n==1
handles.a=handles.currdat(:,:,handles.ii+1);
axes(handles.axes1)
imshow(mat2gray(handles.a))

Ath=handles.a>handles.M+handles.trs*handles.I;
CC = bwconncomp(Ath,8);
sizz=size(Ath);

a=[1:length(CC.PixelIdxList)];
for i=1:length(CC.PixelIdxList)
    a(i)=length(CC.PixelIdxList{i});
end
histfin=zeros(sizz(1),sizz(2));
for i=1:length(CC.PixelIdxList)
    if length(CC.PixelIdxList{i}) > handles.minov
       a=CC.PixelIdxList{1,i};
       for j=1:length(a)
           [X1,Y1] = ind2sub([sizz(1),sizz(2)],a(j));
           histfin(X1,Y1)=1;
       end
    end
end

axes(handles.axes2)
imshow(histfin)

else
handles.a=handles.currdat(:,:,handles.ii+1);
axes(handles.axes1)
imshow(mat2gray(handles.a))
end


% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
trs=round(get(hObject,'Value'));
handles.trs=trs;
set(handles.text2,'string',num2str(trs));

Ath=handles.a>handles.M+trs*handles.I;
CC = bwconncomp(Ath,8);
sizz=size(Ath);

a=[1:length(CC.PixelIdxList)];
for i=1:length(CC.PixelIdxList)
    a(i)=length(CC.PixelIdxList{i});
end
histfin=zeros(sizz(1),sizz(2));
for i=1:length(CC.PixelIdxList)
    if length(CC.PixelIdxList{i}) > handles.minov
       a=CC.PixelIdxList{1,i};
       for j=1:length(a)
           [X1,Y1] = ind2sub([sizz(1),sizz(2)],a(j));
           histfin(X1,Y1)=1;
       end
    end
end

axes(handles.axes2)
imshow(histfin)

handles.n=1;
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
assignin('base','trs1',handles.trs)
assignin('base','ovl1',handles.minov)



% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
minov=round(get(hObject,'Value'));
handles.minov=minov;
set(handles.text3,'string',num2str(minov));


Ath=handles.a>handles.M+handles.trs*handles.I;
CC = bwconncomp(Ath,8);
sizz=size(Ath);

a=[1:length(CC.PixelIdxList)];
for i=1:length(CC.PixelIdxList)
    a(i)=length(CC.PixelIdxList{i});
end
histfin=zeros(sizz(1),sizz(2));
for i=1:length(CC.PixelIdxList)
    if length(CC.PixelIdxList{i}) > handles.minov
       a=CC.PixelIdxList{1,i};
       for j=1:length(a)
           [X1,Y1] = ind2sub([sizz(1),sizz(2)],a(j));
           histfin(X1,Y1)=1;
       end
    end
end

axes(handles.axes2)
imshow(histfin)

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function text2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function text3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
