function [] = show_figs3(histfin,histfin2)

sizz=size(histfin);

parfor i=1:sizz(3)
L(:,:,i) = imresize(histfin(:,:,i),0.25,'bilinear');
U(:,:,i) = imresize(histfin2(:,:,i),0.25,'bilinear');
end

sizz=size(L);
figure; hold on;
ph=patch(isosurface(1:sizz(1),1:sizz(1),1:sizz(3),L));
set(ph, 'FaceColor', [0 0 1], 'EdgeColor', 'none','FaceAlpha',0.5);
pl=patch(isosurface(1:sizz(1),1:sizz(1),1:sizz(3),U));
set(pl, 'FaceColor', [1 0 0], 'EdgeColor', 'none','FaceAlpha',0.5);