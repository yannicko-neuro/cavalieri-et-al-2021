function [histfin2,overlap,overlap_div,thressum,redsum,greensum,numCom] = comp_overlap2(histfin,histfinpt, minovl, maxovl, xsiz, ysiz, zsiz)

green=histfin;
red=histfinpt;
threscom=green.*red;
sizz=size(threscom);
CC = bwconncomp(threscom,18);

for i=1:length(CC.PixelIdxList)
    a(i)=length(CC.PixelIdxList{i});
end

histfin2=zeros(sizz(1),sizz(2),sizz(3));
for i=1:length(CC.PixelIdxList)
    if length(CC.PixelIdxList{i}) > minovl && length(CC.PixelIdxList{i}) < maxovl
       a=CC.PixelIdxList{1,i};
       for k=1:length(a)
           [X1,Y1,Z1] = ind2sub([sizz(1),sizz(2),sizz(3)],a(k));
           histfin2(X1,Y1,Z1)=1;
       end
    end
end

sii=xsiz*ysiz*zsiz;

redsum=sum(sum(sum(sum(red))))*sii;
greensum=sum(sum(sum(sum(green))))*sii;
thressum=redsum/sum(sum(sum(sum(histfin2(:)))))*sii;
overlap=sum(sum(sum(sum(histfin2))))*sii;
overlap_div=overlap/sizz(3);

numCom=0;
for i=1:length(CC.PixelIdxList)
    if length(CC.PixelIdxList{i}) > minovl && length(CC.PixelIdxList{i}) < maxovl
       numCom=numCom+1;
    end
end




