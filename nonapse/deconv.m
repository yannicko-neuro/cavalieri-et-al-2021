for i=1:length(handles.fileIndex)
FileTif=handles.fileIndex(i).name;
FinalImage{1,i} = FileTif;
InfoImage=imfinfo(FileTif);
mImage=InfoImage(1).Width;
nImage=InfoImage(1).Height;
NumberImages=length(InfoImage);
FinalImage2=zeros(nImage,mImage,NumberImages,'uint8');

TifLink = Tiff(FileTif, 'r');
for k=1:NumberImages
   TifLink.setDirectory(k);
   FinalImage2(:,:,k)=TifLink.read();
end
TifLink.close();
FinalImage{2,i}=FinalImage2;

end


for i=1:length(handles.fileIndex)
  PSF = fspecial('gaussian',1,10);
  INITPSF = ones(size(PSF));
  [J{i} P{i}] = deconvblind(FinalImage{2,i},INITPSF);
end
