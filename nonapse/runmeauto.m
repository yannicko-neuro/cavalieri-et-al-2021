clear all;
%set parameters
%put stacks into seperate folder. first folder should be the one containing
%the neuron or whatever you want to check the target on
%number of stacks to compare either 2 or 3
handles2.comp=2;

%make manual selection of thresholds
manual=0;

%minimum overlap comparison 1
handles2.ovlmin1=8;
%maximum overlap
handles2.ovlmax1=700;

%minimum overlap comparison 2
handles2.ovlmin2=8;
%maximum overlap
handles2.ovlmax2=700;

%size of voxel x,y and z
handles2.xsiz=0.058;
handles2.ysiz=0.058;
handles2.zsiz=0.400;


%other values; don't touch
handles2.files=[];
handles2.fileIndex=[];
handles2.directory_name=[];
handles2.J={};
handles2.shearl=1;
handles2.meanden=0;
handles2.sigma=0.65;
handles2.vals6={};
handles2.histfin={};
handles2.histfin2={};
handles2.overlap={};
handles2.overlap_div={};
handles2.thressum={};
handles2.greensum={};
handles2.redsum={};
addpath('shearlet_toolbox');

%define data
for i=1:handles2.comp
handles2.directory_name=uigetdir;
handles2.files=dir(handles2.directory_name);
handles2.fileIndex = dir(fullfile(handles2.directory_name,'*.tif') );
addpath(handles2.directory_name);

for j=1:length(handles2.fileIndex)
FinalImage{j,i}=loadTiffStack(handles2.fileIndex(j).name);
end
end
disp('Data Loading Done');

for j=1:length(FinalImage(:,1))
    
for i=1:handles2.comp
PSF = fspecial('gaussian',7,10);
INITPSF = ones(size(PSF));
[handles2.J{i} P{i}] = deconvblind(FinalImage{j,i},INITPSF);
end

disp('Deconvolution Done');


for i=1:handles2.comp
     handles2.vals6{i}=denoising_shear2(handles2.sigma,handles2.J{i});
     %handles2.J{i}=0;
end

disp('Denoising Done');


%for i=1:handles2.comp
%     handles2.vals6{i}=medfilt3(handles2.vals6{i},[3 3 3]);
%end

trss=[4,2];
ovll=[75,23]; 
    



for i=1:handles2.comp
    trss2=trss(i);
    ovl2=ovll(i);
    handles2.histfin{i}=thresh(handles2.vals6{i},i,ovl2,trss2);
    handles2.vals6{i}=0;
end
disp('Thresholding Done');


if handles2.comp==2
[handles2.histfin2{1},overlap,overlap_div,thressum,redsum,greensum,numCom] = comp_overlap2(handles2.histfin{1},handles2.histfin{2}, handles2.ovlmin1, handles2.ovlmax1, handles2.xsiz, handles2.ysiz, handles2.zsiz);

Overlap=transpose(overlap);
Overlap_Divided=transpose(overlap_div);
RedSum=transpose(redsum);
GreenSum=transpose(greensum);
ThresSum=transpose(thressum);

disp('Results Computed');

data(1,:)=Overlap;
data(2,:)=Overlap_Divided;
data(3,:)=RedSum;
data(4,:)=GreenSum;
data(5,:)=ThresSum;
data(6,:)=trss(1);
data(7,:)=trss(2);
data(8,:)=ovll(1);
data(9,:)=ovll(2);
data(10)=numCom;

S1=handles2.fileIndex(j).name;
SS = S1(4:end-4);
SSS='_1.csv';
St=strcat(SS,SSS);
csvwrite(St,data)

show_figs3(handles2.histfin{1},handles2.histfin2{1});
S1=handles2.fileIndex(j).name;
SS = S1(4:end-4);
SSS='.fig';
St=strcat(SS,SSS);
savefig(St)
close all;
disp('Data and Figures Saved');
end



if handles2.comp==3
    
[handles2.histfin2{1},overlap,overlap_div,thressum,redsum,greensum,numCom] = comp_overlap2(handles2.histfin{1},handles2.histfin{2}, handles2.ovlmin1, handles2.ovlmax1, handles2.xsiz, handles2.ysiz, handles2.zsiz);

data=[];
Overlap=transpose(overlap);
Overlap_Divided=transpose(overlap_div);
RedSum=transpose(redsum);
GreenSum=transpose(greensum);
ThresSum=transpose(thressum);

disp('Results Computed');

data(1,:)=Overlap;
data(2,:)=Overlap_Divided;
data(3,:)=RedSum;
data(4,:)=GreenSum;
data(5,:)=ThresSum;
data(6,:)=trss(1);
data(7,:)=trss(2);
data(8,:)=trss(3);
data(9,:)=ovll(1);
data(10,:)=ovll(2);
data(11,:)=ovll(3);
data(12,:)=numCom;


S1=handles2.fileIndex(j).name;
SS = S1(4:end-4);
SSS='_1.csv';
St=strcat(SS,SSS);
csvwrite(St,data)

show_figs3(handles2.histfin{1},handles2.histfin2{1});
S1=handles2.fileIndex(j).name;
SS = S1(4:end-4);
SSS='.fig';
St=strcat(SS,SSS);
savefig(St)
close all;
disp('Data and Figures Saved');

[handles2.histfin2{1},overlap,overlap_div,thressum,redsum,numCom] = comp_overlap2(handles2.histfin{1},handles2.histfin{3}, handles2.ovlmin2, handles2.ovlmax2, handles2.xsiz, handles2.ysiz, handles2.zsiz);
data=[];
Overlap=transpose(overlap);
Overlap_Divided=transpose(overlap_div);
RedSum=transpose(redsum);
GreenSum=transpose(greensum);
ThresSum=transpose(thressum);

disp('Results Computed');

data(1,:)=Overlap;
data(2,:)=Overlap_Divided;
data(3,:)=RedSum;
data(4,:)=GreenSum;
data(5,:)=ThresSum;
data(6,:)=trss{1};
data(7,:)=trss{2};
data(8,:)=trss{3};
data(9,:)=ovll{1};
data(10,:)=ovll{2};
data(11,:)=ovll{3};
data(12,:)=numCom;


SS = S1(4:end-4);
SSS='_2.csv';
St=strcat(SS,SSS);
csvwrite(St,data)

show_figs3(handles2.histfin{1},handles2.histfin2{1});
S1=handles2.fileIndex(j).name;
SS = S1(4:end-4);
SSS='_2.fig';
St=strcat(SS,SSS);
savefig(St)
close all;
disp('Data and Figures Saved');

end

end
