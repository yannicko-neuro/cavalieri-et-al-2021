function [histfin] = thresh(II,fileName,ovlmin,trss2)
II=im2double(II);
sizz=size(II);
M=median(II(:));
I=iqr(II(:));


%change to make thresholding more conservative or lenient
Ath=II>M+trss2*I;

CC = bwconncomp(Ath,26);

TF = fileName;
if TF~=1
    a=[1:length(CC.PixelIdxList)];
for i=1:length(CC.PixelIdxList)
    a(i)=length(CC.PixelIdxList{i});
end
histfin=zeros(sizz(1),sizz(2),sizz(3));
for i=1:length(CC.PixelIdxList)
    if length(CC.PixelIdxList{i}) > ovlmin
       a=CC.PixelIdxList{1,i};
       for j=1:length(a)
           [X1,Y1,Z1] = ind2sub([sizz(1),sizz(2),sizz(3)],a(j));
           histfin(X1,Y1,Z1)=1;
       end
    end
end
else
a=[];
for i=1:length(CC.PixelIdxList)
    a(i)=length(CC.PixelIdxList{i});
end
mainneuron=find(a==max(a));
    
       histfin=zeros(sizz(1),sizz(2),sizz(3));
       a=CC.PixelIdxList{1,mainneuron};
       for j=1:length(a)
           [X1,Y1,Z1] = ind2sub([sizz(1),sizz(2),sizz(3)],a(j));
           histfin(X1,Y1,Z1)=1;
       end
end
end


