function [] = show_figs2(histfin,histfin2)
figure; hold on;

sizz=size(histfin);

ph=patch(isosurface(1:sizz(1),1:sizz(1),1:sizz(3),histfin));
pl=patch(isosurface(1:sizz(1),1:sizz(1),1:sizz(3),histfin2));
set(pl, 'FaceColor', [1 0 0], 'EdgeColor', 'none','FaceAlpha',0.5);