function varargout = pNoNapse(varargin)
% PNONAPSE MATLAB code for pNoNapse.fig
%      PNONAPSE, by itself, creates a new PNONAPSE or raises the existing
%      singleton*.
%
%      H = PNONAPSE returns the handle to a new PNONAPSE or the handle to
%      the existing singleton*.
%
%      PNONAPSE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PNONAPSE.M with the given input arguments.
%
%      PNONAPSE('Property','Value',...) creates a new PNONAPSE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before pNoNapse_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to pNoNapse_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help pNoNapse

% Last Modified by GUIDE v2.5 07-Oct-2016 13:06:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @pNoNapse_OpeningFcn, ...
                   'gui_OutputFcn',  @pNoNapse_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before pNoNapse is made visible.
function pNoNapse_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pNoNapse (see VARARGIN)
addpath('shearlet_toolbox');
handles.files=[];
handles.fileIndex=[];
handles.directory_name=[];
handles.J={};
handles.shearl=1;
handles.meanden=0;
handles.sigma=0.65;
handles.vals6={};
handles.histfin={};
handles.ovlmin=8;
handles.ovlmax=700;
handles.xsiz=0.0600663;
handles.ysiz=0.0600663;
handles.zsiz=0.3952000;
handles.histfin2={};
handles.overlap={};
handles.overlap_div={};
handles.thressum={};
handles.greensum={};
handles.redsum={};


% Choose default command line output for pNoNapse
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes pNoNapse wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = pNoNapse_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.directory_name=uigetdir;
handles.files=dir(handles.directory_name);
handles.fileIndex = dir(fullfile(handles.directory_name,'*.tif') );
addpath(handles.directory_name);

guidata(hObject, handles);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
for i=1:length(handles.fileIndex)
FileTif=handles.fileIndex(i).name;
FinalImage{1,i} = FileTif;
InfoImage=imfinfo(FileTif);
mImage=InfoImage(1).Width;
nImage=InfoImage(1).Height;
NumberImages=length(InfoImage);
FinalImage2=zeros(nImage,mImage,NumberImages,'uint8');

TifLink = Tiff(FileTif, 'r');
for k=1:NumberImages
   TifLink.setDirectory(k);
   FinalImage2(:,:,k)=TifLink.read();
end
TifLink.close();
FinalImage{2,i}=FinalImage2;

end

h = waitbar(0,'Please wait...');
parfor i=1:length(handles.fileIndex)
  PSF = fspecial('gaussian',23,10);
  %%replace with 7-13
  INITPSF = ones(size(PSF));
  [J{i} P{i}] = deconvblind(FinalImage{2,i},INITPSF);
end

close(h) 

'deconvolution done'
handles.J=J;
guidata(hObject, handles);


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.shearl=get(hObject,'Value');
guidata(hObject, handles);


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = waitbar(0,'Please wait...');

if handles.shearl==1
  parfor i=1:length(handles.fileIndex)
    vals6{i}=denoising_shear2(handles.sigma,handles.J{i});
    end
    handles.vals6=vals6;
end
if handles.meanden==1
end
'denoising done'

close(h) 

guidata(hObject, handles);


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

h = waitbar(0,'Please wait...');

for i=1:length(handles.fileIndex)
    histfin{i}=thresh(handles.vals6{i}, handles.fileIndex(i).name, handles.ovlmin);
end
handles.histfin=histfin;
'thresholding done'

close(h) 
guidata(hObject, handles);


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = waitbar(0,'Please wait...');

b=length(handles.fileIndex)/2;
for i=1:b
[histfin2{i},overlap(i),overlap_div(i),thressum(i),redsum(i),greensum(i)] = comp_overlap2(handles.histfin{i},handles.histfin{i+b}, handles.ovlmin, handles.ovlmax, handles.xsiz, handles.ysiz, handles.zsiz);
end
handles.histfin2=histfin2;
handles.overlap=overlap;
handles.overlap_div=overlap_div;
handles.thressum=thressum;
handles.greensum=greensum;
handles.redsum=redsum;
'compute done'

close(h) 
guidata(hObject, handles);


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
switch get(handles.listbox1, 'Value')
    case 1
prompt = 'Input new Sigma (default 0.65)';
answer = inputdlg(prompt);
handles.sigma = str2num(answer{:});
    case 2
prompt = 'Input new Median filter (default 3 pixel])';
answer = inputdlg(prompt);
handles.medianval = str2num(answer{:});
    case 3
prompt = 'Input minimum Overlay (default <8 voxel) (voxel=0.0014mm^3)';
answer = inputdlg(prompt);
handles.ovlmin = str2num(answer{:});
    case 4
prompt = 'Input maximum Overlay (default >700 voxels (voxel=0.0014mm^3)';
answer = inputdlg(prompt);
handles.ovlmax = str2num(answer{:});
    case 5
prompt = 'Input size of x-voxel';
answer = inputdlg(prompt);
handles.xsiz = str2num(answer{:});
    case 6
prompt = 'Input size of y-voxel)';
answer = inputdlg(prompt);
handles.ysiz = str2num(answer{:});
    case 7
prompt = 'Input size of z-voxel';
answer = inputdlg(prompt);
handles.zsiz = str2num(answer{:});
    otherwise
end
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

h = waitbar(0,'Please wait...');

handles2.decon_picture=handles.J;
handles2.denoi_picture=handles.vals6;
handles2.threshol_picture=handles.histfin;
handles2.overlap_picture=handles.histfin2;
handles2.overlap=handles.overlap;
handles2.overlap_div=handles.overlap_div;
handles2.thressum=handles.thressum;
handles2.greensum=handles.greensum;
handles2.redsum=handles.redsum;
handles2.xsiz=handles.xsiz;
handles2.ysiz=handles.ysiz;
handles2.zsiz=handles.zsiz;
%%handles2.meanden=handles.meanden;
handles2.sigma=handles.sigma;
handles2.ovlmin=handles.ovlmin;
handles2.ovlmax=handles.ovlmax;
handles2.fileIndex=handles.fileIndex;
[upperPath, deepestFolder, ~] = fileparts(handles.directory_name);
fname=deepestFolder;

Overlap=transpose(handles2.overlap);
Overlap_Divided=transpose(handles2.overlap_div);
RedSum=transpose(handles2.redsum);
GreenSum=transpose(handles2.greensum);
ThresSum=transpose(handles2.thressum);
    
b=length(handles.fileIndex)/2;
for i=1:b
    Stack{1,i}=handles2.fileIndex(i).name;
end
Stack=transpose(Stack);

T = table(Stack,Overlap,Overlap_Divided,RedSum,GreenSum,ThresSum);

filename = 'e12IQR5.xlsx';
writetable(T,filename,'Sheet',1,'Range','A1')

save(fname, 'handles2','-v7.3') 

close(h) 

guidata(hObject,handles)


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt = 'Which Stack do you want to see?';
answer = inputdlg(prompt);
stack = str2num(answer{:});
b=length(handles.fileIndex)/2;
show_figs2(handles.histfin{b+stack},handles.histfin2{stack});
guidata(hObject,handles)


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt = 'Which Stack do you want to see?';
answer = inputdlg(prompt);
stack = str2num(answer{:});
b=length(handles.fileIndex)/2;
show_figs3(handles.histfin{stack+b},handles.histfin2{stack});
guidata(hObject,handles)
