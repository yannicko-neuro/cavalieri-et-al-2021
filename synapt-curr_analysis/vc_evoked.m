clearvars -except answer1        
%%
nSweeps = 10; %number of recorded sweeps
baseline_points = 1000; %for Rs calculation
Step= 0.01; %Volts 

onset = 5; %percentage of current to calculate onset

%Store information
prompt1={'Enter file name (under 2018_... form)  : ','Enter event type (ex. eEPSC)  : ', 'What is the polarity of the events? (+ or -)  : '};
title1 = 'Load trace'; dims1=[1,50];
if exist('answer1','var') 
    if ~isempty(answer1)
    definput1= {answer1{1},'',''}; 
    end
else
    definput1 = {'2017_','',''}; 
end
answer1 = inputdlg(prompt1,title1,dims1,definput1);


fileinfo.TraceName = answer1{1};
fileinfo.Protocol = answer1{2};
polarity = answer1{3};

load(uigetfile(strcat('*',answer1{1},'-',answer1{2},'*'))) %load file from cd %load file from cd

%% Extract traces

%rearrange sweeps in one matrix
traces= who('Trace_*'); %identify loaded files from DAT
TraceName = traces{1}; %name of a trace

if length(TraceName) == 15,
    TraceNameStart=TraceName(1:11); %first part of trace name
else
    TraceNameStart=TraceName(1:10); %first part of trace name
end

%rearrange sweeps from pairs to 1 by 1 in the matrix
lengthSweep = length(eval(strcat(TraceNameStart,num2str(1),'_1'))); %duration of the sweep
for ind = 1:nSweeps,
    temp = eval(strcat(TraceNameStart,num2str(ind),'_1')); 
    if polarity == '-',
        evokSweep(:,ind)= -temp(1:lengthSweep,2); %sweep
    else
        evokSweep(:,ind)= temp(1:lengthSweep,2); %sweep
    end
end

% extract mean trace and stim trace

%mean trace of evoked currents
evokSweep2 = evokSweep - median(evokSweep);
mean_trace = mean(evokSweep2,2);
mean_trace= mean_trace-median(mean_trace(1:baseline_points)); %mean evoked trace and zeroed

stim = eval(strcat(TraceNameStart,num2str(ind),'_2')); %stim trace and time vector
stim(:,1) = stim(:,1)-stim(1,1); %time vector starting from zero 
T = diff(stim(1:2,1)); %sampling period in sec
sr = 1/T; %sampling rate in Herz


%clearvars Trace*

%% Rs calculation

% check if no trace saturates
Satur = zeros(1,nSweeps);
for i=1:nSweeps,
    if length(find(evokSweep(:,i) == max(evokSweep(:,i))))>2 & sum(diff(find(evokSweep(:,i) == max(evokSweep(:,i))))==1)>10;
    Satur(i) =1;
    disp (strcat('SWEEP #',num2str(i),' saturates - DISCARDED'))
    end
end
evokSweep(:,Satur==1) = [];
nSweeps=length(evokSweep(1,:)); %update number of sweeps

%find baseline
evokSweep_baseline = zeros(1,nSweeps);
for i=1:nSweeps,
    evokSweep_baseline(i) = mean(evokSweep(1:baseline_points,i));
end

%find Rs
Rs = zeros(nSweeps,1);
for i=1:nSweeps,
    if polarity == '+'
    [curr, time] = findpeaks(-evokSweep(1:2000,i)+evokSweep_baseline(i),'MinPeakHeight',200*10^(-12),'NPeaks',1);
    else
    [curr, time] = findpeaks(evokSweep(1:2000,i)-evokSweep_baseline(i),'MinPeakHeight',200*10^(-12),'NPeaks',1);
    end
    Rs(i)=(Step/curr)/10^6; %values in MegaOhms
end   

mean_Rs = mean(Rs);
RsDisplay = ['mean Rs = ', num2str(mean_Rs),' MOhms']; disp (RsDisplay);

%Checks values of Rs
if sum(Rs > 30)~=0,
    disp ('*********       Series Resistance TOO HIGH            *********')
    exc1 = find(Rs > 30);
    disp ('Steps exceeding 30 MOhms:')
    disp (num2str(exc1))
    nSweeps = nSweeps- length(exc1); %number of sweeps without saturation
    %evokSweep(:,exc1) = []; %new matrix with non_saturating traces
end
nSweeps=length(evokSweep(1,:)); %update number of sweeps


%Checks values of Rs to be constant
if sum(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))~=0,
    disp ('*********       Series Resistance VARIES MORE than 20%            *********')
    exc2 = find(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))+3;
    disp ('Steps varying more than 20%:')
    disp (num2str(exc2))
    disp ('DISCARDED')
    %nSweeps = nSweeps- length(exc2); %number of sweeps that pass 20% thresh
    exc=unique([exc1; exc2]);
    evokSweep(:,exc(exc<=nSweeps)) = []; %new matrix with ok Rs traces
end
nSweeps=length(evokSweep(1,:)); %update number of sweeps
% extract mean trace and stim trace

%mean trace of evoked currents
mean_trace = mean(evokSweep,2);
mean_trace= mean_trace-median(mean_trace(1:baseline_points)); %mean evoked trace and zeroed

%% extract mean trace and stim trace
%stim trace
[~,Tstim] = findpeaks(stim(:,2),'NPeaks',1,'SortStr','descend'); %time of electric stimulation
Tstim = sort(Tstim);
%Tstim=4146

%Store information
fileinfo.SamplingRate = sr;
fileinfo.Traces = evokSweep;
fileinfo.StimTrace = stim(:,2);


%Calculate maximum 
figure, plot(mean_trace);
DoublePeak = inputdlg('Is there a double peak? (y/n) = ','');
if DoublePeak{1} == 'n';
    %[amp0, temp0] = findpeaks(mean_trace(Tstim(1):Tstim(1)+500),'SortStr','descend','Npeaks',1); %detect peaks in trace
    [amp0, tempo] = findpeaks(mean_trace(Tstim(1)+10:Tstim(1)+500),'SortStr','descend','Npeaks',1); %detect peaks in trace
    temp0 = Tstim(1)+tempo+9; %time index on whole trace
else 
    temp = mean_trace(Tstim(1)+50:Tstim(1)+500); %segment containing peaks
    lm = islocalmax(temp,'MaxNumExtrema',2); %find 2 local maxima
    amp0 = temp(find(lm~=0,1)); %time point of local maxima
    temp0 =  Tstim(1)+49+find(lm~=0,1); %time index on whole trace
end

bl0 = mean(mean_trace(Tstim(1)-50:Tstim(1))); %baseline1
peak0 = amp0 - bl0; %amplitude figur = peak - baseline

Rise = mean_trace(Tstim+5:temp0); %ramping phase of event
%Rise = mean_trace(Tstim+20:temp0); %ramping phase of event
Rise_tv = (0:1/sr:length(Rise)/sr-1/sr)'; %time vector of rise time

%Calculate onset and delay
Ionset = (peak0/100)*onset; %value of current at onset
ind_closest = dsearchn(Rise,Ionset); %closest point to I onset
delay = ind_closest*1000/sr; %delay in ms

%
delayDisplay = ['Delay to onset = ', num2str(delay), ' ms']; disp (delayDisplay);

%Calculate Rise Time and Tau
Rise10 = (peak0/100)*10; %value of current at 10% peak
Rise90 = (peak0/100)*90; %value of current at 90% peak
ind_Rise10 = dsearchn(Rise,Rise10); %closest point to 10% peak
ind_Rise90 = dsearchn(Rise,Rise90); %closest point to 90% peak
RiseTime = (ind_Rise90-ind_Rise10)*1000/sr; %Rise Time in ms

if DoublePeak{1} == 'n';
        if lengthSweep >10000,
            decay = mean_trace(temp0:temp0+4000);%in sec, DELAY from stim peak and evoked peak
        else
            decay = mean_trace(temp0:temp0+2350);%in sec, DELAY from stim peak and evoked peak
        end
elseif DoublePeak{1} == 'y',
    temp2 =  find(temp==max(temp)); %time index on whole trace
    if lengthSweep >10000,
    decay = mean_trace(Tstim(1)+50+temp2:Tstim(1)+50+temp2+4000);%in sec, DELAY from stim peak and evoked peak
    else
    decay = mean_trace(Tstim(1)+50+temp2:Tstim(1)+50+temp2+2350);%in sec, DELAY from stim peak and evoked peak    
    end
    peak0 = max(temp);
end

decay_tv = (0:1/sr:length(decay)/sr-1/sr)'; %time vector of decay
decay90 = dsearchn(decay,peak0/100*90);
decay10 = dsearchn(decay,peak0/100*10);
decay37 = dsearchn(decay,peak0/100*37);
DecayTime = abs(decay37-decay90)*1000/sr;
HalfWidth = (abs(length(Rise)- dsearchn(Rise,peak0/100*50)) +abs(dsearchn(decay,peak0/100*50))) *1000/sr;    
Area = trapz([Rise_tv(ind_Rise10:end) ;Rise_tv(end)+decay_tv(1:decay10)], [Rise(ind_Rise10:end); decay(1:decay10)])*10^15;

close all
%%
ans = 'n';
bounds = {'90','10'};
while ans == 'n'
    decay_bottom = dsearchn(decay,peak0/100*str2double(bounds{2}));
    decay_top = dsearchn(decay,peak0/100*str2double(bounds{1}));
    f = ezfit(decay_tv(decay_top:decay_bottom), decay(decay_top:decay_bottom),'exp'); %exponential fit
    tau = -1/f.m(2)*1000;  %time costant of decay
    figure, plot(decay_tv,decay,'k'), showfit(f, 'dispeqboxmode','off');
    xlabel('Time (ms)'), ylabel('Current (pA)'),title('Mean trace and stimulation')             

    ans_cell = inputdlg('Are you satisfied with the fit? (y/n)','');
    ans = ans_cell{1};
    if ans =='n';
        bounds = inputdlg({'Upper boundary of fit (75 to 99)?','Lower boundary of fit (1 to 20)?'},'',[1 35],bounds);
    end
    close
end

%%
%PLOT & STORE
figure,
subplot(211), plot(stim(:,1)*1000, evokSweep*10^12), title('Overlay')
subplot(223),plot(stim(:,1)*1000,stim(:,2)*10,'r')
            hold on, plot(stim(:,1)*1000,mean_trace*10^12,'k'), xlim([190 490])
            hold on, plot((temp0-1)*1000/sr,amp0*10^12,'b*')
            hold on, plot(xlim, [Ionset*10^12 Ionset*10^12],'g--');
            hold on, plot(1000*(ind_closest+Tstim+19)/sr, mean_trace(ind_closest+Tstim+19)*10^12,'bo')
            %hold on, plot(1000*(ind_closest+Tstim+9)/sr, mean_trace(ind_closest+Tstim+9)*10^12,'bo');
            %hold on, plot(1000*(ind_closest+Tstim+4)/sr, mean_trace(ind_closest+Tstim+4)*10^12,'bo');

            xlabel('Time (ms)'), ylabel('Current (pA)'),title('Mean trace and stimulation')

subplot(224),plot(decay_tv,decay,'k'), showfit(f, 'dispeqboxmode','off');
            hold on, plot(decay_tv(decay10),(peak0/100*10),'ro')
            hold on, plot(decay_tv(decay90),(peak0/100*90),'ro')
            xlabel('Time (ms)'), ylabel('Current (pA)'),title('Mean trace and stimulation')
           
%%
%Print Results 
PeakDisp = ['Peak Amplitude = ', num2str(peak0*10^12), ' pA']; disp (PeakDisp);
AreaDisp = ['Area = ', num2str(Area), ' pA*ms']; disp (AreaDisp);

RiseTimeDisplay = ['Rise time 10-90% = ', num2str(RiseTime), ' ms']; disp (RiseTimeDisplay);
DecayTimeDisplay = ['Decay time 90-37% = ', num2str(DecayTime), ' ms']; disp (DecayTimeDisplay);
HwDisplay = ['Half-width = ', num2str(HalfWidth),' ms']; disp (HwDisplay);
tauDisplay = ['Decay time constant (Tau) = ', num2str(tau)]; disp (tauDisplay);

%store data
fileinfo.Rs = mean_Rs;
fileinfo.Delay = delay;
fileinfo.Peak = peak0*10^12;
fileinfo.Area = Area;
fileinfo.RiseTime = RiseTime;
fileinfo.DecayTime = DecayTime;
fileinfo.HalfWidth = HalfWidth;
fileinfo.tau = tau;

%save file
temp_name = strcat(fileinfo.TraceName,'-',fileinfo.Protocol);
save(strcat(temp_name,'_fileinfo'), 'fileinfo')
savefig(strcat(temp_name,'_fig'))