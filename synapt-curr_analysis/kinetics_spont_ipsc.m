clear all
%%extract mean trace
main_folder='C:\Users\cavalieri\Documents\INMED\Current Experiments\PATCH CLAMP\VC\2019 - VC_ok\Spontaneous\Traces_spont (igor)';

cd(main_folder);
[file,path,indx] = uigetfile('*.txt');
mean_trace0 = importfile(strcat(path,file));
winopen(path);

splitcells=split(path,'\');
birthdate=splitcells{12};
rec=splitcells{13};
event=splitcells{14};

fileinfo.TraceName = rec;
fileinfo.Birthdate = birthdate;
fileinfo.Event = event;

%
temp_name = strcat(fileinfo.TraceName,'-',fileinfo.Event);
filename = strcat(temp_name,'_fileinfo');

if isfile(strcat(path,filename,'.mat'))
    temp = load(strcat(path,filename,'.mat'));% File exists.
    fileinfo_existing= temp.fileinfo;
    disp(['RECORDING ALREADY ANALYSED']);
else
    disp(['fileinfo not present']);  % File does not exist.
end

sr = 10000; %Hz
onset = 10;
if event=='sEPSC',
    mean_trace= -mean_trace0;
elseif event=='sIPSC',
    mean_trace=mean_trace0;
end
tv=(0:1/sr:length(mean_trace)/sr-1/sr)'; 

%%
%Calculate maximum 
[amp0, temp0] = max(mean_trace);

bl0 = mean(mean_trace(temp0-35:temp0-25));%baseline1
%bl0=+2.4
peak0 = amp0 - bl0; %amplitude figur = peak - baseline
%peak0 = amp0
mean_trace= mean_trace -bl0;

%
Rise = mean_trace(1:temp0); %ramping phase of event
Rise_tv = (0:1/sr:length(Rise)/sr-1/sr)'; %time vector of rise time

%Calculate Rise Time 
Rise10 = (peak0/100)*10; %value of current at 10% peak
Rise90 = (peak0/100)*90; %value of current at 90% peak
ind_Rise10 = dsearchn(Rise,Rise10); %closest point to 10% peak
ind_Rise90 = dsearchn(Rise,Rise90); %closest point to 90% peak
RiseTime = (ind_Rise90-ind_Rise10)*1000/sr; %Rise Time in ms

%plot(mean_trace)
figure; subplot(121), plot(mean_trace),subplot(122), plot(flip(0:1:length(mean_trace(1:temp0))-1),mean_trace(1:temp0)) 
%subplot(121), plot(mean_trace),subplot(122), plot(mean_trace(1:temp0)),
hold on, plot(xlim, [Rise10 Rise10],'--r'), plot(length(Rise)-ind_Rise10,Rise(ind_Rise10),'ro')
hold on, plot(xlim, [Rise90 Rise90],'--b')
hold on, plot(xlim, [0 0],'-g'); set(gca,'xdir','reverse')

%%
% %%
% ind_Rise10=length(Rise)-24;
% RiseTime = (ind_Rise90-ind_Rise10)*1000/sr; %Rise Time in ms
% 
% figure; subplot(121), plot(mean_trace),subplot(122), plot(flip(0:1:length(mean_trace(1:temp0))-1),mean_trace(1:temp0)) 
% %subplot(121), plot(mean_trace),subplot(122), plot(mean_trace(1:temp0)),
% hold on, plot(xlim, [Rise10 Rise10],'--r'), plot(length(Rise)-ind_Rise10,Rise(ind_Rise10),'ro')
% hold on, plot(xlim, [Rise90 Rise90],'--b')
% hold on, plot(xlim, [0 0],'-g'); set(gca,'xdir','reverse')


%%
%Calculate Decay time
Decay = mean_trace(temp0:end);%in sec, DELAY from stim peak and evoked peak
Decay_tv = (0:1/sr:length(Decay)/sr-1/sr)'; %time vector of decay
decay90 = dsearchn(Decay,peak0/100*90);
decay10 = dsearchn(Decay,peak0/100*10);
decay37 = dsearchn(Decay,peak0/100*37);
DecayTime = abs(decay37-decay90)*1000/sr;
HalfWidth = (abs(length(Rise)- dsearchn(Rise,peak0/100*50)) +abs(dsearchn(Decay,peak0/100*50))) *1000/sr;    
Area = trapz([Rise_tv(ind_Rise10:end) ;Rise_tv(end)+Decay_tv(1:decay10)], [Rise(ind_Rise10:end); Decay(1:decay10)])*1000;
close all

ans = 'n';
if isfield(fileinfo,'bounds')
    bounds = fileinfo.bounds;
    disp(['Bounds for fit already existing']);  % File does not exist.
else
    bounds = {'95','30'};
end

while ans == 'n'
    decay_bottom = dsearchn(Decay,peak0/100*str2double(bounds{2}));
    decay_top = dsearchn(Decay,peak0/100*str2double(bounds{1}));
    f = ezfit(Decay_tv(decay_top:decay_bottom), Decay(decay_top:decay_bottom),'exp'); %exponential fit
    tau = -1/f.m(2)*1000;  %time costant of decay
    figure; plot(Decay_tv,Decay,'k'), showfit(f, 'dispeqboxmode','off');
    xlabel('Time (ms)'), ylabel('Current (pA)'),title('Mean trace and stimulation')             

    ans_cell = inputdlg('Are you satisfied with the fit? (y/n)','');
    ans = ans_cell{1};
    if ans =='n';
        bounds = inputdlg({'Upper boundary of fit (75 to 99)?','Lower boundary of fit (1 to 20)?'},'',[1 35],bounds);
    end
    close
    fileinfo.bounds=bounds;
end

%
%store data
fileinfo.Area = Area;
fileinfo.RiseTime = RiseTime;
fileinfo.HalfWidth = HalfWidth;
fileinfo.DecayTime = DecayTime;
fileinfo.Tau = tau;


%save file
if ~exist(strcat(path,filename,'.mat'),'file'); %fileinfo not existing
    save(strcat(path,filename), 'fileinfo') %then save file
    disp(['Fileinfo saved']);
elseif exist(strcat(path,filename,'.mat'),'file')==2, %fileinfo already existing
    disp(['Fileinfo already existing']);
    if isequal(fileinfo_existing,fileinfo) %previous and current analysis are the same
        disp(['No change in the analysis']);
        save(strcat(path,filename), 'fileinfo') %then save file
        disp(['Fileinfo saved']);
    else
        ans_cell = inputdlg('Do you really want to OVERWRITE the existing file? (y/n)','');
        ans = ans_cell{1};
        if ans=='y'
            save(strcat(path,filename), 'fileinfo') %then save file
            disp(['Fileinfo OVERWRITTEN']);    
        end
    end    
end


figure, 
subplot(131),plot(tv,mean_trace,'k'),  xlim([0 tv(end)/2]); ylim([min(mean_trace)-2 max(mean_trace)+2]) 
subplot(132), plot(Rise) 
hold on, plot(xlim, [Rise10 Rise10],'--r'), plot(ind_Rise10,Rise(ind_Rise10),'ro')
hold on, plot(xlim, [Rise90 Rise90],'--b')
hold on, plot(xlim, [0 0],'-g'); ylim([min(mean_trace)-2 max(mean_trace)+2]) 
subplot(133), plot(Decay_tv,Decay,'k'), showfit(f, 'dispeqboxmode','off'), xlim([0 Decay_tv(end)/2]);ylim([min(mean_trace)-2 max(mean_trace)+2]) 


savefig(strcat(path,temp_name,'_fig'))