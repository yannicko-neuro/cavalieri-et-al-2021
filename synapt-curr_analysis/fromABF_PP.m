%% all ABF
fp = '\\netdata\EqpCossart\Davide Cavalieri\Experiments\PATCH CLAMP\VC\__RAW_FILES\2017_10_18\';
fn = '2017_10_18_0002.abf';
f=abfload(strcat(fp,fn));

F2 = f(:,1)/10;
figure, plot(F2)

%% single traces
fn = '2017_10_18_0002_IPSC_PP.abf';
f=abfload(fn);

f2 = f(:,1)*10/gain;
figure, plot(f2)

%%
%%
%Store information
prompt1={'Enter file name (under 2018_... form)  : ','Enter event type (ex. eEPSC)  : ', 'What is the polarity of the events? (+ or -)  : '};
title1 = 'Load trace'; dims1=[1,50];
definput1 = {'2019_','',''}; 
answer1 = inputdlg(prompt1,title1,dims1,definput1);


fileinfo.TraceName = answer1{1};
fileinfo.Protocol = answer1{2};
polarity = answer1{3};


nSweeps=10;
lengthSweep = 11200;
%gain = 5;

sr = 20000;
baseline_points = 1000; %for Rs calculation
Step= 0.01; %Volts 

onset = 5; %percentage of current to calculate onset


%% 
findpeaks(-f2,'MinPeakProminence',400,'Npeaks',10,'MinPeakDistance',100000);

[~,locs]= findpeaks(-f2,'MinPeakProminence',400,'Npeaks',10,'MinPeakDistance',100000);
%%
evokSweep = zeros(lengthSweep,nSweeps);
for i=1:nSweeps
    if polarity=='+'
    evokSweep(:,i)=f2(locs(i)-500:locs(i)+lengthSweep-501);
    else
    evokSweep(:,i)=-f2(locs(i)-500:locs(i)+lengthSweep-501);
    end
    figure, plot(evokSweep(:,i))
end


mean_trace = mean(evokSweep,2);
mean_trace= mean_trace-median(mean_trace(1:baseline_points)); %mean evoked trace and zeroed
figure, plot(mean_trace)

stim = zeros(length(mean_trace),2);
stim(:,1) = (0:1/sr:length(mean_trace)/sr-1/sr)';

%%
% check if no trace saturates
Satur = zeros(1,nSweeps);
for i=1:nSweeps,
    if length(find(evokSweep(:,i) == max(evokSweep(:,i))))>2 & sum(diff(find(evokSweep(:,i) == max(evokSweep(:,i))))==1)>5;
    Satur(i) =1;
    disp (strcat('SWEEP #',num2str(i),' saturates - DISCARDED'))
    end
end

nSweeps = length(Satur(Satur==0)); %number of sweeps without saturation
evokSweep = evokSweep(:,find(Satur==0)); %new matrix with non_saturating traces

%find baseline
evokSweep_baseline = zeros(1,nSweeps);
for i=1:nSweeps,
    evokSweep_baseline(i) = mean(evokSweep(1:baseline_points,i));
end


%find Rs
Rs = zeros(nSweeps,1);
if polarity == '+'
    for i=1:nSweeps
    [curr, time] = findpeaks(-evokSweep(1:1800,i)+evokSweep_baseline(i),'MinPeakHeight',200,'NPeaks',1);
    Rs(i)=(Step/curr*10^12)/10^6; %values in MegaOhms
    end
elseif polarity == '-'
    for i=1:nSweeps
    [curr, time] = findpeaks(evokSweep(1:1800,i)-evokSweep_baseline(i),'MinPeakHeight',200,'NPeaks',1);
    Rs(i)=(Step/curr*10^12)/10^6; %values in MegaOhms
    end
end   

mean_Rs = mean(Rs);
RsDisplay = ['mean Rs = ', num2str(mean_Rs),' MOhms']; disp (RsDisplay);

%Checks values of Rs
if sum(Rs > 30)~=0;
    disp ('*********       Series Resistance TOO HIGH            *********')
    exc = find(Rs > 30);
    disp ('Steps exceeding 30 MOhms:')
    disp (num2str(exc))
    disp ('DISCARDED')
    
    nSweeps = nSweeps- length(exc); %number of sweeps without saturation
    evokSweep(:,exc) = []; %new matrix with non_saturating traces
end



%Checks values of Rs to be constant
if sum(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))~=0,
    disp ('*********       Series Resistance VARIES MORE than 20%            *********')
    exc = find(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))+3;
    disp ('Steps varying more than 20%:')
    disp (num2str(exc))
    disp ('DISCARDED')
    nSweeps = nSweeps- length(exc); %number of sweeps that pass 20% thresh
    evokSweep(:,exc) = []; %new matrix with ok Rs traces
end


%%
%Store information
fileinfo.SamplingRate = sr;
fileinfo.Traces = evokSweep;
fileinfo.StimTrace = stim(:,2);

%%
%Calculate maximum 
%[~,Tstim] = findpeaks(stim(:,2),'NPeaks',2,'SortStr','descend'); %time of electric stimulation
%Tstim = sort(Tstim);$

t_after_stim = 50; %point to take from stimulus to search for max
% t_after_stim = 100;

%detection on first peak
figure, plot(mean_trace);
DoublePeak = inputdlg({'Is there a double peak in the first pulse? (y/n) = ','Is there a double peak in the second pulse? (y/n) = '},'');
if DoublePeak{1} == 'n';
    [amp1, temp1] = findpeaks(mean_trace(Tstim(1):Tstim(1)+500),'SortStr','descend','Npeaks',1); %detect peaks in trace
    temp1 = Tstim(1)+temp1; %time index on whole trace
else
    temp = mean_trace(Tstim(1)+t_after_stim:Tstim(1)+500); %segment containing peaks
    %lm = islocalmax(temp,'MaxNumExtrema',2); %find 2 local maxima
    lm = islocalmax(temp,'MaxNumExtrema',3); %find 2 local maxima
    amp1 = temp(find(lm~=0,1)); %time point of local maxima
    temp1 =  Tstim(1)+t_after_stim-1+find(lm~=0,1); %time index on whole trace
end
bl1 = mean(mean_trace(Tstim(1)-t_after_stim:Tstim(1))); %baseline1
peak1 = amp1 - bl1; %amplitude  = peak - baseline

%detection on second peak
[bl2,temp_bl2]= findpeaks(-mean_trace(Tstim(2)+10:Tstim(2)+10+500),'SortStr','descend','Npeaks',1); %find baseline2
bl2 = -bl2;
temp_bl2 = Tstim(2)+temp_bl2+9; 

if DoublePeak{2} == 'n';
    %[amp2, temp2] = findpeaks(mean_trace(Tstim(2):Tstim(2)+500),'SortStr','descend','Npeaks',1); %detect peaks in trace
    [amp2, temp2] = findpeaks(mean_trace(Tstim(2)+10:Tstim(2)+10+500),'SortStr','descend','Npeaks',1); %detect peaks in trace

    temp2 = Tstim(2)+temp2+9; %time index on whole trace
else
    temp = mean_trace(Tstim(2)+t_after_stim:Tstim(2)+500); %segment containing peaks
    lm = islocalmax(temp,'MaxNumExtrema',2); %find 2 local maxima
    %lm = islocalmax(temp,'MaxNumExtrema',3); %find 3 local maxima
    amp2 = temp(find(lm~=0,1)); %time point of local maxima
    temp2 =  Tstim(2)+t_after_stim-1+find(lm~=0,1); %time index on whole trace
end

peak2 = amp2 - bl2; %amplitude  = peak - baseline


close all
%%
%Store information
PPratio = peak2/peak1;
PPDisplay = ['Paired Pulse Ratio = ', num2str(PPratio)]; disp (PPDisplay);

fileinfo.meanRs = mean_Rs;
fileinfo.Traces = evokSweep;
fileinfo.FirstPeak = amp1;
fileinfo.SecondPeak = amp2;
fileinfo.PP = PPratio;

%
%PLOT & Save

f = figure; 
subplot(211), plot(stim(:,1)*1000, evokSweep), title('Overlay')
subplot(223),plot(stim(:,1)*1000,stim(:,2)*10-100,'r')
            hold on, plot(stim(:,1)*1000,mean_trace,'k'), xlim([90 200])
            hold on, plot(xlim, [bl1 bl1],'g--');
            hold on, plot(temp1*1000/sr,amp1,'g*')
            hold on, plot(xlim, [bl2 bl2],'b--');
            hold on, plot(temp2*1000/sr,amp2,'b*')
            %hold on, plot(1000*(ind_closest+Tstim)/sr, mean_trace(ind_closest+Tstim)*10^12,'bo');
            xlabel('Time (ms)'), ylabel('Current (pA)'),title('Mean trace and stimulation')

%save file
temp_name = strcat(fileinfo.TraceName,'-',fileinfo.Protocol);
save(strcat(temp_name,'_fileinfo'), 'fileinfo')
savefig(f,strcat(temp_name,'_fig'))