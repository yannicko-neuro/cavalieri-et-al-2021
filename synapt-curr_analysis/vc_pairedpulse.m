clearvars -except answer1

%%

%Store information
prompt1={'Enter file name (under 2018_... form)  : ','Enter event type (ex. eEPSC)  : ', 'What is the polarity of the events? (+ or -)  : '};
title1 = 'Load trace'; dims1=[1,50];
if exist('answer1','var') 
    if ~isempty(answer1)
    definput1= {answer1{1},'',''}; 
    end
else
    definput1 = {'2019_','',''}; 
end

answer1 = inputdlg(prompt1,title1,dims1,definput1);

fileinfo.TraceName = answer1{1};
fileinfo.Protocol = answer1{2};
polarity = answer1{3};

load(uigetfile(strcat('*',answer1{1},'-',answer1{2},'*'))) %load file from cd %load file from cd

%% Extract traces

baseline_points = 1000; %for Rs calculation
Step= 0.01; %Volts 
onset = 5; %percentage of current to calculate onset


%rearrange sweeps in one matrix
traces= who('Trace_*'); %identify loaded files from DAT
traceName = traces{1}; %name of a trace

nSweeps = length(traces)/2; %number of recorded sweeps

if length(traceName) == 15,
TraceNameStart=traceName(1:11); %first part of trace name
else
TraceNameStart=traceName(1:10); %first part of trace name
end

%rearrange sweeps from pairs to 1 by 1 in the matrix
lengthSweep = length(eval(strcat(TraceNameStart,num2str(1),'_1'))); %duration of the sweep
evokSweep = zeros(lengthSweep,nSweeps); %empty matrix for sweeps

for ind = 1:nSweeps,
    temp = eval(strcat(TraceNameStart,num2str(ind),'_1')); 
    if polarity == '-',
        evokSweep(:,ind)= -temp(1:lengthSweep,2); %sweep
    else
        evokSweep(:,ind)= temp(1:lengthSweep,2); %sweep
    end
end

stim = eval(strcat(TraceNameStart,num2str(ind),'_2')); %stim trace and time vector
stim(:,1) = stim(:,1)-stim(1,1); %time vector starting from zero 
T = diff(stim(1:2,1)); %sampling period in sec
sr = 1/T; %sampling rate in Herz


%clearvars Trace*

%% Rs calculation

% check if no trace saturates
Satur = zeros(1,nSweeps);
for i=1:nSweeps,
    if length(find(evokSweep(:,i) == max(evokSweep(:,i))))>2 & sum(diff(find(evokSweep(:,i) == max(evokSweep(:,i))))==1)>10;
    Satur(i) =1;
    disp (strcat('SWEEP #',num2str(i),' saturates - DISCARDED'))
    end
end
evokSweep(:,Satur==1) = [];
nSweeps=length(evokSweep(1,:)); %update number of sweeps

%find baseline
evokSweep_baseline = zeros(1,nSweeps);
for i=1:nSweeps,
    evokSweep_baseline(i) = mean(evokSweep(1:baseline_points,i));
end

%find Rs
Rs = zeros(nSweeps,1);
for i=1:nSweeps,
    if polarity == '+'
    [curr, time] = findpeaks(-evokSweep(1:2000,i)+evokSweep_baseline(i),'MinPeakHeight',200*10^(-12),'NPeaks',1);
    else
    [curr, time] = findpeaks(evokSweep(1:2000,i)-evokSweep_baseline(i),'MinPeakHeight',200*10^(-12),'NPeaks',1);
    end
    Rs(i)=(Step/curr)/10^6; %values in MegaOhms
end   

mean_Rs = mean(Rs);
RsDisplay = ['mean Rs = ', num2str(mean_Rs),' MOhms']; disp (RsDisplay);

%Checks values of Rs
if sum(Rs > 30)~=0,
    disp ('*********       Series Resistance TOO HIGH            *********')
    exc1 = find(Rs > 30);
    disp ('Steps exceeding 30 MOhms:')
    disp (num2str(exc1))
    nSweeps = nSweeps- length(exc1); %number of sweeps without saturation
    %evokSweep(:,exc1) = []; %new matrix with non_saturating traces
end
nSweeps=length(evokSweep(1,:)); %update number of sweeps


%Checks values of Rs to be constant
if sum(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))~=0,
    disp ('*********       Series Resistance VARIES MORE than 20%            *********')
    exc2 = find(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))+3;
    disp ('Steps varying more than 20%:')
    disp (num2str(exc2))
    disp ('DISCARDED')
    %nSweeps = nSweeps- length(exc2); %number of sweeps that pass 20% thresh
    if exist('exc1','var') 
        exc=unique([exc1; exc2]);
        evokSweep(:,exc(exc<=nSweeps)) = []; %new matrix with ok Rs traces
    else
        evokSweep(:,exc2) = []; %new matrix with ok Rs traces
    end
end
nSweeps=length(evokSweep(1,:)); %update number of sweeps
% extract mean trace and stim trace

%mean trace of evoked currents
evokSweep2 = evokSweep - median(evokSweep);
mean_trace = mean(evokSweep2,2);
mean_trace= mean_trace-median(mean_trace(1:baseline_points)); %mean evoked trace and zeroed


%%
%Calculate maximum 
[~,Tstim] = findpeaks(stim(:,2),'NPeaks',2,'SortStr','descend'); %time of electric stimulation
Tstim = sort(Tstim);
%%
t_after_stim = 50; %point to take from stimulus to search for max
% t_after_stim = 100;

%detection on first peak
figure, plot(mean_trace);
DoublePeak = inputdlg({'Is there a double peak in the first pulse? (y/n) = ','Is there a double peak in the second pulse? (y/n) = '},'');
if DoublePeak{1} == 'n'
    [amp1, temp1] = findpeaks(mean_trace(Tstim(1):Tstim(1)+500),'SortStr','descend','Npeaks',1); %detect peaks in trace
    temp1 = Tstim(1)+temp1; %time index on whole trace
else
    temp = mean_trace(Tstim(1)+t_after_stim:Tstim(1)+500); %segment containing peaks
    lm = islocalmax(temp,'MaxNumExtrema',2); %find 2 local maxima
    %lm = islocalmax(temp,'MaxNumExtrema',3); %find 2 local maxima
    amp1 = temp(find(lm~=0,1)); %time point of local maxima
    temp1 =  Tstim(1)+t_after_stim-1+find(lm~=0,1); %time index on whole trace
end
bl1 = mean(mean_trace(Tstim(1)-t_after_stim:Tstim(1))); %baseline1
peak1 = amp1 - bl1; %amplitude  = peak - baseline

%detection on second peak
[bl2,temp_bl2]= findpeaks(-mean_trace(Tstim(2)+10:Tstim(2)+10+500),'SortStr','descend','Npeaks',1); %find baseline2
bl2 = -bl2;
temp_bl2 = Tstim(2)+temp_bl2+9; 

if DoublePeak{2} == 'n';
    %[amp2, temp2] = findpeaks(mean_trace(Tstim(2):Tstim(2)+500),'SortStr','descend','Npeaks',1); %detect peaks in trace
    [amp2, temp2] = findpeaks(mean_trace(Tstim(2)+10:Tstim(2)+10+500),'SortStr','descend','Npeaks',1); %detect peaks in trace

    temp2 = Tstim(2)+temp2+9; %time index on whole trace
else
    temp = mean_trace(Tstim(2)+t_after_stim:Tstim(2)+500); %segment containing peaks
    lm = islocalmax(temp,'MaxNumExtrema',2); %find 2 local maxima
    %lm = islocalmax(temp,'MaxNumExtrema',3); %find 3 local maxima
    amp2 = temp(find(lm~=0,1)); %time point of local maxima
    temp2 =  Tstim(2)+t_after_stim-1+find(lm~=0,1); %time index on whole trace
end

peak2 = amp2 - bl2; %amplitude  = peak - baseline


close all
%%
%Store information
PPratio = peak2/peak1;
PPDisplay = ['Paired Pulse Ratio = ', num2str(PPratio)]; disp (PPDisplay);

fileinfo.meanRs = mean_Rs;
fileinfo.Traces = evokSweep;
fileinfo.FirstPeak = amp1;
fileinfo.SecondPeak = amp2;
fileinfo.PP = PPratio;

%
%PLOT & Save

f = figure; 
subplot(211), plot(stim(:,1)*1000, evokSweep*10^12), title('Overlay')
subplot(223),plot(stim(:,1)*1000,stim(:,2)*10-100,'r')
            hold on, plot(stim(:,1)*1000,mean_trace*10^12,'k'), xlim([190 490])
            hold on, plot(xlim, [bl1*10^12 bl1*10^12],'g--');
            hold on, plot(temp1*1000/sr,amp1*10^12,'g*')
            hold on, plot(xlim, [bl2*10^12 bl2*10^12],'b--');
            hold on, plot(temp2*1000/sr,amp2*10^12,'b*')
            %hold on, plot(1000*(ind_closest+Tstim)/sr, mean_trace(ind_closest+Tstim)*10^12,'bo');
            xlabel('Time (ms)'), ylabel('Current (pA)'),title('Mean trace and stimulation')

%save file
temp_name = strcat(fileinfo.TraceName,'-',fileinfo.Protocol);
save(strcat(temp_name,'_fileinfo'), 'fileinfo')
savefig(f,strcat(temp_name,'_fig'))