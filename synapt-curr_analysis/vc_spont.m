clearvars -except answer1

%%

%Store information
prompt1={'Enter file name (under 2018_... form)  : ','Enter event type (ex. eEPSC)  : ', 'What is the polarity of the events? (+ or -)  : '};
title1 = 'Load trace'; dims1=[1,50];
definput1 = {'2018_','',''}; 
answer1 = inputdlg(prompt1,title1,dims1,definput1);

fileinfo.TraceName = answer1{1};
fileinfo.Protocol = answer1{2};
polarity = answer1{3};

load(uigetfile(strcat('*',answer1{1},'-',answer1{2},'*'))) %load file from cd

%% Extract traces
nSweeps = 9; %number of recorded sweeps
baseline_points = 1000; %for Rs calculation
Step= 0.01; %Volts 

Fs = 10000; %Herz
T = 1/Fs; %seconds

lengthSweep = 200000; %duration of the sweep
spontSweep = zeros(lengthSweep,nSweeps); %empty matrix for sweeps


sweep=2;
x=spontSweep(600:end,sweep);
%%

%% Fast Fourier transform

clearvars -except x
Fs= 10000; %frame rate
T = 1/Fs; %period
tv = T:T:length(x)/Fs; %time vector

nfft=2^nextpow2(length(x)); %zero padding 
freqX=fft(x,nfft); %frequency domain of the signal
freqX = freqX(1:nfft/2); %half of frequency domain
amplX= log(abs(freqX)); %amplitude values, only real numbers
f = (0:nfft/2-1)*Fs/nfft; %frequencies

%% Filtering 

%notch filter
wo = 50/(Fs/2);
bw = wo/35;
[b,a]=iirnotch(wo,bw);
fvtool(b,a)


% low pass filter
order = 32; %number for order ofimpulse reponse
cut_off=1.0e3/Fs/2; %cutoff frequency, norm by Nyquist fr
lp = fir1(order,cut_off);
con = conv(x,lp);

%bp = fir1(order,[97 103]/Fs/2,'bandpass');
%con = conv(con,bp);

X2=fft(con,nfft);
X2 = X2(1:nfft/2);

%%
figure, 
subplot(411),plot(x(80000:88000))
subplot(412),plot(f,abs(X),'b'),xlim([0 600]), ylim([0 1*10^-7])
%subplot(212),plot(f,abs(X2),'r')
subplot(413),plot(con(80000:88000))
subplot(414),plot(f,abs(X2),'b'),xlim([0 600]), ylim([0 1*10^-7])

%% Extract traces

%rearrange sweeps in one matrix
traces= who('Trace_*'); %identify loaded files from DAT
TraceName = traces{1}; %name of a trace

if length(TraceName) == 15,
    TraceNameStart=TraceName(1:11); %first part of trace name
else
    TraceNameStart=TraceName(1:10); %first part of trace name
end

%rearrange sweeps from pairs to 1 by 1 in the matrix
for ind = 1:nSweeps,
    temp = eval(strcat(TraceNameStart,num2str(ind),'_1')); 
    if polarity == '-',
        spontSweep(:,ind)= -temp(1:lengthSweep,2); %sweep
    else
        spontSweep(:,ind)= temp(1:lengthSweep,2); %sweep
    end
    
end

%%
%clearvars Trace*

%% Rs calculation
 
% % check if no trace saturates
 Satur = zeros(1,nSweeps);
 for i=1:nSweeps
     if length(find(spontSweep(:,i) == max(spontSweep(:,i))))>2 & sum(diff(find(spontSweep(:,i) == max(spontSweep(:,i))))==1)>10;
     Satur(i) =1;
     disp (strcat('SWEEP #',num2str(i),' saturates - DISCARDED'))
     end
 end
% 
nSweeps = length(Satur(Satur==0)); %number of sweeps without saturation
spontSweep = spontSweep(:,find(Satur==0)); %new matrix with non_saturating traces

%find baseline
spontSweep_baseline = zeros(1,nSweeps);
for i=1:nSweeps,
    spontSweep_baseline(i) = mean(spontSweep(1:baseline_points,i));
end

%find Rs
Rs = zeros(nSweeps,1);
for i=1:nSweeps;
    if polarity == '+'
    [curr, time] = findpeaks(-spontSweep(1:2000,1)+spontSweep_baseline(i),'MinPeakHeight',200*10^(-12),'NPeaks',1);
    else
    [curr, time] = findpeaks(spontSweep(1:2000,1)-spontSweep_baseline(i),'MinPeakHeight',200*10^(-12),'NPeaks',1);
    end
    Rs(i)=(Step/curr)/10^6; %values in MegaOhms
end   

mean_Rs = mean(Rs);
RsDisplay = ['mean Rs = ', num2str(mean_Rs),' MOhms']; disp (RsDisplay);

%Checks values of Rs
if sum(Rs > 30)~=0;
    disp ('*********       Series Resistance TOO HIGH            *********')
    exc = find(Rs > 30);
    disp ('Steps exceeding 30 MOhms:')
    disp (num2str(exc))
    disp ('DISCARDED')
    
    nSweeps = nSweeps- length(exc); %number of sweeps without saturation
    spontSweep(:,exc) = []; %new matrix with non_saturating traces
end



%Checks values of Rs to be constant
if sum(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))~=0,
    disp ('*********       Series Resistance VARIES MORE than 20%            *********')
    exc = find(or(Rs(4:end) > mean(Rs(1:3))+mean(Rs(1:3))/100*20, Rs(4:end) < mean(Rs(1:3))-mean(Rs(1:3))/100*20))+3;
    disp ('Steps varying more than 20%:')
    disp (num2str(exc))
end

% %
% provat = tv(2000:10000);
% prova = spontSweep(2000:10000,1);
% 
% figure, plot(provat, prova)
% hold on, findpeaks(prova,provat,'MinPeakHeight',5,'MinPeakProminence',5,'MinPeakDistance',0.01)
% ylim([-200,200]), xlim([min(provat),max(provat)])


